#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PySide import QtGui
import sys
import xmpp
from datetime import datetime


# zrobić tłumaczenia komunikatów


class Menu(QtGui.QWidget):

    def __init__(self, akcje, parent=None):
        self.akcje = akcje
        super(Menu, self).__init__(parent)
        self.init_ui()

    def init_ui(self):
        toolbar = QtGui.QToolBar('toolbar')
        toolbar.addAction(self.akcje[1])
        toolbar.addAction(self.akcje[0])

        layout = QtGui.QHBoxLayout()
        layout.addWidget(toolbar)
        self.setLayout(layout)


class Kontakt(QtGui.QWidget):

    def __init__(self, avatar, nick, opis, polaczenie_xmpp, parent=None):
        self.polaczenie_xmpp = polaczenie_xmpp
        self.avatar = avatar
        self.nick = nick
        self.opis = opis
        super(Kontakt, self).__init__(parent)
        self.init_ui()

    def init_ui(self):
        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.avatar)
        layout.addWidget(self.nick)
        layout.addWidget(self.opis)
        self.setLayout(layout)

    def mouseDoubleClickEvent(self, event):
        # http://stackoverflow.com/questions/8478210/how-to-create-a-new-window-button-pyside-pyqt
        window = QtGui.QMainWindow(self)
        window.setWindowTitle('Wzorce1')
        window.setCentralWidget(OknoRozmowyQt(self.avatar, self.nick, self.opis, self.polaczenie_xmpp))
        window.show()


class Roster(QtGui.QWidget):

    def __init__(self, polaczenie_xmpp, parent=None):
        self.polaczenie_xmpp = polaczenie_xmpp
        super(Roster, self).__init__(parent)
        self.init_ui()

    def init_ui(self):
        roster = QtGui.QScrollArea()

        layout = QtGui.QHBoxLayout()
        layout.addWidget(roster)

        self.list_widget = QtGui.QListWidget(self)

        for i in self.polaczenie_xmpp.roster():
            avatar = QtGui.QLabel("av")
            nick = QtGui.QLabel(i)
            opis = QtGui.QLabel("opis")
            kontakt = Kontakt(avatar, nick, opis, self.polaczenie_xmpp)

            item = QtGui.QListWidgetItem()
            item.setSizeHint(kontakt.sizeHint())
            self.list_widget.addItem(item)
            self.list_widget.setItemWidget(item, kontakt)

        roster.setWidget(self.list_widget)
        self.setLayout(layout)

    def dodaj(self, kontakt):
        item = QtGui.QListWidgetItem()
        item.setSizeHint(kontakt.sizeHint())
        self.list_widget.addItem(item)
        self.list_widget.setItemWidget(item, kontakt)


class PoleTekstowe(QtGui.QWidget):

    def __init__(self, adres, polaczenie_xmpp, widzet_wiadomosci, parent=None):
        self.adres = adres
        self.polaczenie_xmpp = polaczenie_xmpp
        self.widzet_wiadomosci = widzet_wiadomosci
        super(PoleTekstowe, self).__init__(parent)
        self.init_ui()

    def init_ui(self):
        self.wpisywanie = QtGui.QLineEdit()
        przycisk = QtGui.QPushButton('wyślij')

        QtCore.QObject.connect(przycisk, QtCore.SIGNAL('clicked()'), self.wyslij_wiadomosc)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.wpisywanie)
        layout.addWidget(przycisk)
        self.setLayout(layout)

    def wyslij_wiadomosc(self):
        if self.wpisywanie.text():
            self.polaczenie_xmpp.wyslij(self.adres, self.wpisywanie.text())
            self.widzet_wiadomosci.nowy_wpis("ja", self.wpisywanie.text(), QtCore.Qt.red)
            self.wpisywanie.clear()


class Status(QtGui.QWidget):

    def __init__(self, parent=None):
        super(Status, self).__init__(parent)
        self.init_ui()

    def init_ui(self):
        status = QtGui.QLabel('status')

        layout = QtGui.QHBoxLayout()
        layout.addWidget(status)
        self.setLayout(layout)


class OknoQt(QtGui.QWidget):

    def linia_pozioma(self):
        linia = QtGui.QFrame()
        linia.setFrameShape(QtGui.QFrame.HLine)
        return linia


class OknoGlowneQt(OknoQt):

    def __init__(self, polaczenie_xmpp):
        self.polaczenie_xmpp = polaczenie_xmpp

        app = QtGui.QApplication.instance() or QtGui.QApplication(sys.argv)
        super(OknoGlowneQt, self).__init__()
        self.init_ui()
        sys.exit(app.exec_())

    def init_ui(self):
        #_ = Qt()  # bez przypisania nie działa

        exit_action = QtGui.QAction(QtGui.QIcon(), '&Wyjdź', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.triggered.connect(self.close)

        nowa_rozmowa = QtGui.QAction(QtGui.QIcon(), '&Rozmowa', self)
        nowa_rozmowa.setShortcut('Ctrl+N')
        nowa_rozmowa.triggered.connect(self.nowa_rozmowa)

        akcje = [exit_action, nowa_rozmowa]

        menu = Menu(akcje)
        self.roster = Roster(self.polaczenie_xmpp)
        # wpisywanie = PoleTekstowe()
        status = Status()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(menu)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(self.roster)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(status)
        self.setLayout(layout)

        self.setWindowTitle('Wzorce')
        self.show()

    def nowa_rozmowa(self):
        # http://stackoverflow.com/questions/8478210/how-to-create-a-new-window-button-pyside-pyqt
        window = QtGui.QMainWindow(self)
        window.setWindowTitle('Wzorce2')
        window.setCentralWidget(OknoNowejRozmowyQt(self.polaczenie_xmpp, window, self.roster))
        window.show()


class OknoNowejRozmowyQt(OknoQt):
    def __init__(self, polaczenie_xmpp, parent, roster):
        self.polaczenie_xmpp = polaczenie_xmpp
        self.parent = parent
        self.roster = roster

        super(OknoNowejRozmowyQt, self).__init__()
        self.init_ui()

    def init_ui(self):
        przycisk = QtGui.QPushButton('Rozmawiaj')
        przycisk.clicked.connect(self.przelacz_okno)

        self.nick = QtGui.QLineEdit()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(QtGui.QLabel("Rozmówca: "))
        layout.addWidget(self.nick)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(przycisk)
        self.setLayout(layout)

        self.setWindowTitle('Wzorce3')
        self.show()

    def przelacz_okno(self):
        self.close()
        self.parent.close()

        xmpp.Roster().dodaj(self.nick.text())

        avatar = QtGui.QLabel("av")
        nick = QtGui.QLabel(self.nick.text())
        opis = QtGui.QLabel("opis")
        kontakt = Kontakt(avatar, nick, opis, self.polaczenie_xmpp)
        self.roster.dodaj(kontakt)

        window = QtGui.QMainWindow(self)
        window.setWindowTitle('Wzorce4')
        window.setCentralWidget(OknoRozmowyQt(None, self.nick, None, self.polaczenie_xmpp))
        window.show()


class Rozmowca(QtGui.QWidget):

    def __init__(self, nick, parent=None):
        self.nick = nick
        super(Rozmowca, self).__init__(parent)
        self.init_ui()

    def init_ui(self):
        layout = QtGui.QHBoxLayout()

        # avatar = QtGui.QLabel("av")
        nick = QtGui.QLabel(self.nick)
        nick.setAlignment(QtCore.Qt.AlignCenter)
        # opis = QtGui.QLabel("opis")
        # kontakt = Kontakt(avatar, nick, opis)

        layout.addWidget(nick)
        self.setLayout(layout)


class Rozmowa(QtGui.QWidget):

    def __init__(self, parent=None):
        super(Rozmowa, self).__init__(parent)
        self.init_ui()

    def init_ui(self):
        rozmowa = QtGui.QScrollArea()

        rozmowa.setMinimumSize(QtCore.QSize(400, 300))

        layout = QtGui.QHBoxLayout()
        layout.addWidget(rozmowa)

        pomocniczy_widget = QtGui.QWidget()
        self.vbox = QtGui.QVBoxLayout(pomocniczy_widget)
        self.vbox.setSizeConstraint(QtGui.QLayout.SetFixedSize)
        rozmowa.setWidget(pomocniczy_widget)
        self.setLayout(layout)

    def nowy_wpis(self, nick, tresc, kolor):
        wpis = QtGui.QLabel("{} {} {}".format(datetime.now(), nick, tresc))
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Foreground, kolor)
        wpis.setPalette(palette)
        self.vbox.addWidget(wpis)


class OknoRozmowyQt(OknoQt):

    def __init__(self, avatar, nick, opis, polaczenie_xmpp):
        self.polaczenie_xmpp = polaczenie_xmpp
        self.nick = nick.text()

        super(OknoRozmowyQt, self).__init__()
        self.init_ui()

    def init_ui(self):
        rozmowca = Rozmowca(self.nick)
        wiadomosci = Rozmowa()
        wpisywanie = PoleTekstowe(self.nick, self.polaczenie_xmpp, wiadomosci)
        self.rozmowa_xmpp = xmpp.RozmowaXMPP(self.nick, self.polaczenie_xmpp, wiadomosci)
        status = Status()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(rozmowca)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(wiadomosci)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(wpisywanie)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(status)
        self.setLayout(layout)

        self.setWindowTitle('Rozmowa')
        self.show()


class OknoLogowaniaQt(OknoQt):

    def __init__(self, dane_logowania):
        self.dane_logowania = dane_logowania

        app = QtGui.QApplication(sys.argv)
        super(OknoLogowaniaQt, self).__init__()
        self.init_ui()
        app.exec_()

    def init_ui(self):
        zaloguj = QtGui.QPushButton('Zaloguj')
        zaloguj.clicked.connect(self.przelacz_okno)

        self.login = QtGui.QLineEdit()
        self.haslo = QtGui.QLineEdit()
        self.haslo.setEchoMode(QtGui.QLineEdit.Password)
        self.szyfrowanie = QtGui.QCheckBox('Szyfruj połączenie')
        self.szyfrowanie.toggle()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(QtGui.QLabel("Login: "))
        layout.addWidget(self.login)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(QtGui.QLabel("Hasło: "))
        layout.addWidget(self.haslo)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(QtGui.QLabel("Bezpieczeństwo: "))
        layout.addWidget(self.szyfrowanie)
        layout.addWidget(self.linia_pozioma())
        layout.addWidget(zaloguj)
        self.setLayout(layout)

        self.setWindowTitle('Wzorce')
        self.show()

    def przelacz_okno(self):
        self.close()

        self.dane_logowania.ustaw(
            self.login.text(),
            self.haslo.text(), self.szyfrowanie.isChecked())


class PasekPostepuQt(QtGui.QWidget):

    def __init__(self, akcja):
        app = QtGui.QApplication.instance() or QtGui.QApplication(sys.argv)
        super(PasekPostepuQt, self).__init__()

        layout = QtGui.QVBoxLayout(self)

        # Create a progress bar and a button and add them to the main layout
        self.pasek = QtGui.QProgressBar(self)
        self.pasek.setRange(0, 1)
        layout.addWidget(self.pasek)
        przycisk = QtGui.QPushButton("Anuluj", self)
        layout.addWidget(przycisk)

        przycisk.clicked.connect(self.on_cancel)

        self.akcja = Logowanie(akcja)

        self.pasek.setRange(0, 0)
        self.akcja.start()

        self.akcja.taskFinished.connect(self.on_finished)

        self.setLayout(layout)
        self.setWindowTitle('Łączenie')
        self.show()

        app.exec_()

    def on_cancel(self):
        self.close()

    def on_finished(self):
        self.close()


from PySide import QtCore


class Logowanie(QtCore.QThread):
    taskFinished = QtCore.Signal()

    def __init__(self, akcja):
        super(Logowanie, self).__init__()
        self.akcja = akcja

    def run(self):
        self.akcja()
        self.taskFinished.emit()

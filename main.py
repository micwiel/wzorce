#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import okna
import stan
import logging
import threading
import time
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', action='store_true')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-q', '--qt', action='store_true')
    group.add_argument('-g', '--gtk', action='store_true')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(module)s %(levelname)s %(message)s')

    if args.qt:
        lib = "qt"
    elif args.gtk:
        lib = "gtk"

    dane_logowania = stan.DaneLogowania()

    okna.OknoLogowania(lib, dane_logowania)

    polaczenie_xmpp = stan.PolaczenieXMPP(dane_logowania)

    t = threading.Thread(target=polaczenie_xmpp.start, args=())
    t.daemon = True
    t.start()

    def logowanie():
        licznik = 0
        while licznik < 5:
            if polaczenie_xmpp.sprawdz_poprawnosc():
                return True
            time.sleep(1)
            licznik += 1

    okna.PasekPostepu(lib, logowanie)

    if polaczenie_xmpp.sprawdz_poprawnosc():
        okna.OknoGlowne(lib, polaczenie_xmpp)

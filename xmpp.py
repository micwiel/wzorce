#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import nbxmpp
from gi.repository import GObject
import os
from PySide import QtCore
import pickle


class KlientXMPP:

    def __init__(self, jid, haslo, szyfrowanie=True):
        self.jid = nbxmpp.protocol.JID(jid)
        self.password = haslo
        self.sm = nbxmpp.Smacks(self)  # Stream Management
        self.client_cert = None
        if szyfrowanie:
            self.szyfrowanie = 'tls'
        else:
            self.szyfrowanie = 'plain'

        self.pomyslne_logowanie = False

    def on_auth(self, con, auth):
        if not auth:
            print('could not authenticate!')
            exit()
        print('authenticated using', auth)
        self.pomyslne_logowanie = True

    def on_connected(self, con, con_type):
        print('connected with', con_type)
        auth = self.client.auth(
            self.jid.getNode(),
            self.password,
            resource=self.jid.getResource(),
            sasl=1,
            on_auth=self.on_auth)

    def get_password(self, cb, mech):
        cb(self.password)

    def on_connection_failed(self):
        print('could not connect!')

    def _event_dispatcher(self, realm, event, data):
        pass

    def connect(self):
        idle_queue = nbxmpp.idlequeue.get_idlequeue()
        self.client = nbxmpp.NonBlockingClient(
            self.jid.getDomain(),
            idle_queue,
            caller=self)
        self.con = self.client.connect(
            self.on_connected,
            self.on_connection_failed,
            secure_tuple=(
                self.szyfrowanie,
                '',
                '',
                None,
                None))

    def send_message(self, to_jid, text):
        id_ = self.client.send(
            nbxmpp.protocol.Message(
                to_jid,
                text,
                typ='chat'))
        print('sent message with id', id_)
        #GObject.timeout_add(1000, self.quit)

    def quit(self):
        self.disconnect()
        self.ml.quit()

    def disconnect(self):
        self.client.start_disconnect()

    def sprawdz_logowanie(self):
        return self.pomyslne_logowanie

    def start(self):
        self.connect()

        self.ml = GObject.MainLoop()
        self.ml.run()

    def roster(self):
        self.client.sendPresence()
        return Roster().pobierz()


class RozmowaXMPP:
    def __init__(self, rozmowca, polaczenie_xmpp, widzet_wiadomosci):
        self.rozmowca = rozmowca
        self.polaczenie_xmpp = polaczenie_xmpp
        self.register_handlers()
        self.widzet_wiadomosci = widzet_wiadomosci

    def register_handlers(self):
        self.polaczenie_xmpp.klient_xmpp.client.RegisterHandler('message', self.xmpp_message)

    def xmpp_message(self, con, event):
        type = event.getType()
        fromjid = event.getFrom().getStripped()
        if type in ['message', 'chat', None] and fromjid == self.rozmowca and event.getBody():
            self.widzet_wiadomosci.nowy_wpis(self.rozmowca, event.getBody(), QtCore.Qt.blue)


class Roster:
    def pobierz(self):
        try:
            with open(os.environ['HOME'] + '/.wzorce', 'rb') as f:
                lista_uzytkownikow = pickle.load(f)
        except FileNotFoundError:
            lista_uzytkownikow = set()

        return lista_uzytkownikow

    def dodaj(self, nick):
        lista_uzytkownikow = self.pobierz()
        lista_uzytkownikow.add(nick)
        lista_uzytkownikow = pickle.dumps(lista_uzytkownikow, -1)
        with open(os.environ['HOME'] + '/.wzorce', 'wb') as f:
            f.write(lista_uzytkownikow)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from gi.repository import Gtk


class OknoGtk(Gtk.Window):

    def linia_pozioma(self):
        linia = Gtk.HSeparator()
        return linia


class OknoLogowaniaGtk(OknoGtk):

    def __init__(self, dane_logowania):
        self.dane_logowania = dane_logowania

        super(OknoLogowaniaGtk, self).__init__()
        self.connect("delete-event", Gtk.main_quit)
        self.init_ui()
        Gtk.main()

    def init_ui(self):
        zaloguj = Gtk.Button('Zaloguj')
        zaloguj.connect("clicked", self.przelacz_okno)

        self.login = Gtk.Entry()
        self.haslo = Gtk.Entry()
        self.haslo.set_visibility(False)
        self.szyfrowanie = Gtk.CheckButton('Szyfruj połączenie')
        self.szyfrowanie.set_active(True)

        vbox = Gtk.VBox()
        vbox.add(Gtk.Label("Login: "))
        vbox.add(self.login)
        vbox.add(self.linia_pozioma())
        vbox.add(Gtk.Label("Hasło: "))
        vbox.add(self.haslo)
        vbox.add(self.linia_pozioma())
        vbox.add(Gtk.Label("Bezpieczeństwo: "))
        vbox.add(self.szyfrowanie)
        vbox.add(self.linia_pozioma())
        vbox.add(zaloguj)
        self.add(vbox)

        self.set_title('Wzorce')
        self.show_all()

    def przelacz_okno(self, _):
        self.close()

        self.dane_logowania.ustaw(
            self.login.get_text(),
            self.haslo.get_text(), self.szyfrowanie.get_active())

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import wyjatki
import xmpp


class DaneLogowania:
    def ustaw(self, login, haslo, szyfrowanie):
        if not login or not haslo:
            raise wyjatki.PusteDane
        else:
            self.login = login
            self.haslo = haslo
            self.szyfrowanie = szyfrowanie


class PolaczenieXMPP:
    def __init__(self, dane_logowania):
        self.dane_logowania = dane_logowania
        self.klient_xmpp = xmpp.KlientXMPP(dane_logowania.login, dane_logowania.haslo, dane_logowania.szyfrowanie)

    def sprawdz_poprawnosc(self):
        return self.klient_xmpp.sprawdz_logowanie()

    def start(self):
        self.klient_xmpp.start()

    def zakoncz(self):
        self.klient_xmpp.quit()

    def roster(self):
        return self.klient_xmpp.roster()

    def wyslij(self, jid, msg):
        self.klient_xmpp.send_message(jid, msg)

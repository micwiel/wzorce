#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from qt import OknoGlowneQt, OknoLogowaniaQt, PasekPostepuQt
from pygtk import OknoLogowaniaGtk


class OknoGlowne:

    def __init__(self, lib, polaczenie_xmpp):
        if lib == "qt":
            OknoGlowneQt(polaczenie_xmpp)
        else:
            raise NotImplementedError


class OknoLogowania:

    def __init__(self, lib, dane_logowania):
        if lib == "qt":
            OknoLogowaniaQt(dane_logowania)
        elif lib == "gtk":
            OknoLogowaniaGtk(dane_logowania)
        else:
            raise NotImplementedError


class PasekPostepu:

    def __init__(self, lib, akcja):
#        if lib == "qt":
        PasekPostepuQt(akcja)
#        else:
#            raise NotImplementedError
